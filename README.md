![Build Status](https://gitlab.com/EAL-ITT/20s-itt2-system-development/badges/master/pipeline.svg)


# 20s-ITT2-System-Development

weekly plans, resources and other relevant stuff for the system development lectures in IT technology 2. semester spring.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/20s-itt2-system-development/)
