---
title: '20s ITT2 System Development'
subtitle: 'Lecture plan'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait19, 20S
* Name of lecturer and date of filling in form: RUTR, 2020-01-10
* Title of the course, module or project and ECTS: Embedded Development, 5 ECTS
* Required readings, literature or technical instructions and other background material: programming in Python, the c programming language second edition, AVR achritecture, ATMEL Studion.
* Prior Activities: Students are expected to have mastered all the topics from embedded systems during first semester and only focus on embedded programming, if this is missing; students are more than welcome to receive extra guidance. Extra time should be allocated by the students, to facilitated their missing competences.

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------
RUTR
---------------------------------------------------------

# Information on course structure

Students are expected to use time and strength on learning embedded C. If you have lacking knowledge then you should set extra time on the side and practice/learn/catch up. 

## Lecture Method

We will be doing flip learning, materials and online courses are given by the lecture in advance. In the class the lecture will be doing live coding, that students can not follow, unless they have read the material in advance. The rest of the time will be spend on practical work. The course will be heavily focused on hands-on. The course is divided in two major cycles. First cycle is learning embedded C, second is applying it on a long project. First weeks as shown in the weekly plan are build of knowledge and skills, the last weeks are heavy practical use of skill set.

# General info about the course, module or project
The course will be focusing on practical skill and supplementary theoretical knowledge. Hands on first with basic theory, followed but in-depth theory via focused needs. The students will have all the available information about the course overview from study start. Material will be shared in advance and the students are asked to investigate the topics and prepare from home. In the classroom a short presentation will be given followed by practical work in small groups.

## The student’s learning outcome

The students will focus heavily the first 7 weeks to learn the C programming language and the AVR architecture with the help of ATMEGA series on the ATMEGA328PB‑XMINI. In later weeks the students will create their own prototype board and interface it with a Zigbee communication module.

### Knowledge

Students will focus on IoT knowledge applied on the ATMEGA series and using Zigbee protocol.

### Skills

The students will focus on learning practical skills with focus on applied embedded systems, for example interfacing on the SPI and UART, IoT communication and architecture. Testing their solution and documenting it via gitlab.

### Competencies

Students will be build their competencies on testing and analysis of embedded solutions, taking into account their environmental foot print and compare various technologies.

## Content

The detailed content is given into the weekly plans document. Three major topics are the focus: Learning AVR architecture with C programming, learning IoT architecture and technologies, interfacing various technologies with focus data communication.

## Method

The method of teaching will be a flipped classroom. Students are expected to search and learn on their own in advance following documentation provided by the lecture and their own sources. In the class short power point presentations will be given with focus knowledge and targeting the needs of the classroom. After that the students will be spend their time practicing their skills with the guidance of a lecturer.

## Equipment

See book list for details. A lot of equipment will be needed. Students are advised to form groups.

## Projects with external collaborators  (proportion of students who participated)

There are two projects available. The first one is an IoT system to be build for ActiveDoor using a simple hardware set up. The second is a research and development project with focus on creating articles and research output.

## Test form/assessment
The course includes 1 compulsory elements. See weekly plan for details. The First year end exam will have a simple assignment in which the students will need to interface some simple code and a harder part that is more complicated and students will need to develop a more complicated solution. See exam cataloger for details on compulsory elements, and weekly plans document for tasks planned.

## Other general information
Students are expected to work and ask questions. The course structure may fill that the difficulty oscillates. This is due to the course been build around modules and units of topics.
