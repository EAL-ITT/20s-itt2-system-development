---
Week: 18
Content:  online plan
Material: See links in weekly plan
Initials: RUTR
---


# Google Meet info

We will be using Google Meet for our hangout. The link is:  meet.google.com/jom-faxi-xji

## Goals of the week(s)

We will have a guest lecturer with many years of practical expirience in software design and embedded programming


## Practical goals

1. Listen to a guest lecturer.

 
## Learning goals

1. Real life development and practical work.

## Deliverable

1. Student presentation.

## Schedule

See Time Edit

## Hands-on time

Non

