---
Week: 06
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 06 ITT2 System Development

## Goals of the week(s)

Input and output logics.

### Practical goals

1. Understand input and output using buttons and LEDs.
2. Understand functional programming in C.
3. Understand if, case, for, while.

### Learning goals

1. C programming.
2. GPIO input and output.


### Programming Goals

1. Bitwise Operators
2. Arithmetics Operators
3. Arguments - Call by name
4. Arrays
5. Control Statements - IF, For, While

## Deliverable

1. Create a reaction button to LED interface.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Literature

Students need to read the following chapter from the_c_programming_language_2:

### Chapter 2 and Chapter 3

Students are to read and try out the program language on there own. It is important that students go though it in advance so what we can save time in the classroom.
