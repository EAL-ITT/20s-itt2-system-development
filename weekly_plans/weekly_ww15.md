---
Week: 15
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 15 ITT2 System Development and on words.

## Goals of the week(s)

Presentation to students of UART and PWM solutions. Detail presentation and example via live coding. As well as video directions in tutorial form.

### Practical goals

1. A very in depth UART drivers.
2. In depth presentation of PWM.

### Learning goals

1. Implementation of own communication driver


## Deliverable

1. Communication driver user Serial / UART

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.


