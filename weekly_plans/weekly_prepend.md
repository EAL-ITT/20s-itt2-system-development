---
title: '20S ITT2 System Development'
subtitle: 'Weekly plans'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: '20S ITT2 system development, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and program for each week of the system development classes.
