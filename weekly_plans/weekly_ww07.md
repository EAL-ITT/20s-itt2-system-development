---
Week: 07
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 07 ITT2 System Development

Week 7 and Week 8 topics will be combined as they share a lot of similarities. We will go over the two topics in one go and next week, repeat them as it is vital that this knowledge is solid.

## Goals of the week(s)

Introduction to Interrupt service routines. 

### Practical goals

1. Internal and External interrupt implementation.

### Learning goals

1. C programming.
2. Using interrupts both internal and external.

#### Programming Goals

1. Functions and declarations
2. Return value from function
3. Header files
4. Pointers

## Deliverable

1. Drive inputs with interrupt.
2. Make LED react to interrupt.
3. Split software into headerfiles.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
Students should keep reading and getting better at C programming. Remember to use Linkedin Learning

https://www.linkedin.com/learning/learning-c-5/welcome?u=57075649

https://www.linkedin.com/learning/c-essential-training-1-the-basics/start-your-c-essential-training?u=57075649

https://www.linkedin.com/learning/c-programming-for-embedded-applications/getting-started-in-embedded-systems?u=57075649


# OLA21 - first attempt deadline end of week 8

Make sure you complete all of the points given in Goals for the week as this is your OLA21

