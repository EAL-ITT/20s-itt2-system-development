---
Week: 13
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 13 ITT2 System Development

## Goals of the week(s)

Presentation of interfacing techniques and consideration. Project overview given to the students. 

### Practical goals

1. Tasks for the students.
* Implement sensor / push button on explain board. Connect a button to a GPIO.
* Implement LED for presed button. LED to GPIO, not a LED on the same GPIO as the button.
* Serial Communication from explain to RPI and back. Send button pressed command.
* Register Button presed on the RPI.
* User options for button to turn on and off other LED not - button connected LED. Rested button connected LED.
* Communication LED. Show status with blinking of communication is happening.
2. Using wifi connected the RPI to your network. Old software from first semester can be reused.

### Learning goals

1. C programming.
2. General knowledge around non TCP / IP network.
3. Python sockets.
4. Inerfacing.


## Deliverable

1. Create a link between RPI and ATMEGA using SPI.
2. Create a link between ATMEGA and ATMEGA.


## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments

This will be the last week that we will be using the ATMEGA mini board. From next week we will be creating our own. Students need to have a programmer.

# OLA21 - second attempt

The mandatory assignment is as given below.

1. Using SPI turn an LED on RPI.
2. SPI commend is comming from ATMEGA 328PB
3. Command is generated via a push button and using interrupts.
4. Deliver software diagram.
5. All deliverable are in PDF format and delivered on wiseflow. Email delivery is not accepted.
