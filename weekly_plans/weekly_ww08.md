---
Week: 08
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 08 ITT2 System Development

Week 7 and Week 8 topics will be combined as they share a lot of similarities. We will go over the two topics in one go and next week, repeat them as it is vital that this knowledge is solid.

## Goals of the week(s)

Timers and timers again. Using timers create delay.

### Practical goals

1. AVR timers.
2. AVR clocks.
3. PWM.
4. Controlling periferanls with PWM.

### Learning goals

1. C programming.
2. PWM to motor control.
3. Timers to motor control.

## Deliverable

1. Create a PWM application.
2. Create a timer application.
3. Both outputs need to control a motor drive bridge, test with an osciloscope.


## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Literature

Students need to read the following chapter from the_c_programming_language_2:

### Chapter 4 and Chapter 5

Students are to read and try out the program language on there own. It is important that students go though it in advance so what we can save time in the classroom.


# OLA21 - first attempt deadline end of week 8

This mandatory assignment is build around knowledge from previews week. Students should had completed the assignment last week in class. The mandatory assignment is as given below.

1. Using External Interrupts from a button, turn on a LED.
2. Using Internal interrupt release the LED after 2 seconds.
3. Deliver software in screen shots.
4. Deliver software diagram.
5. All deliverable are in PDF format and delivered on wiseflow. Email delivery is not accepted.
