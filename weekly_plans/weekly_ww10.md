---
Week: 10
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 10

## Goals of the week(s)

I am not in school for weeks 10 and 11. Students will have to work on topics given so far.

### Practical goals

1. Students are expected to test interfacing LED, Buttons and H-bridges.
2. Students are expected that they will work with timers, and interrupts.

### Learning goals

1. C programming.
2. AVR architecture.


## Deliverable

1. Self learning.


## Schedule

See Time Edit

## Hands-on time

Students should try and cover their own needs and cover their missing knowledge.

## Comments
