---
Week: 19
Content:  online plan
Material: See links in weekly plan
Initials: RUTR
---


# Google Meet info

We will be using Google Meet for our hangout. The link is:  meet.google.com/jom-faxi-xji

## Goals of the week(s)

We will have a look again at your work, students need to present their work. Via sharing their screen. And they can receive feedback from the class. There is no - you should have worked or any other kind of critic. Students receive the help they need. 


## Practical goals

1. RUTR shows a the magic of diagrams. Using UML and other tools.
2. Students present what they have done or not done, and everyone helps. 

 
## Learning goals

1. Leaning to present our work online.
2. Structure and feedback.

## Deliverable

1. Student presentation.

## Schedule

See Time Edit

## Hands-on time

None

## Links to Material

https://drive.google.com/open?id=1URmtfL8Zr2P4A2K3C3wAYYROylISLRpC
