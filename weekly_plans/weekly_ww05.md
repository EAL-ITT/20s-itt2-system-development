---
Week: 05
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# General Waring

During this course, you will feel that the tempo is going to fast. You may feel in the first 5-7 weeks that we are rushing though things. This is partially correct.
We are moving fast due to you have basic programming knowledge from python, and knowing all the theory behind the topics we do due to first semester. So you should only use your time and strength on learning embedded C. If you have lacking knowledge then you should set extra time on the side and practice/learn/catch up.

# Week 05 ITT2 System Development

## Goals of the week(s)

See the AVR and ATMEGA architecture, understand atmel studio GPIO and logic of microcontrollers without operating system, barebone programming in C.

### Practical goals

1. Install atmega studio.
2. AVR and ATMEGA understanding and using.
3. C programmings first deep.
4. Make a LED blink.

### Learning goals

1. C programming.
2. AVR registers.
3. ATMEGA GPIO.


### Programming Goals

1. Variables
2. Constants
3. Declarations
4. Functions
5. Define

## Deliverable

1. Create a on off led application, with a timer delay. 

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments
In week 9 we will use the mini board for last time. From week 10 and onward students will need their own programmer. I have around 10 programmers, they are the ATMEL ICE. I advice students to by a cheap programmer to have all the time. The ATMEL ICE is also a debugger and it cost around 1000 dkk.

# Literature

Students need to read the following chapter from the_c_programming_language_2

## Chapter 1 and Chapter 2

Students are to read and try out the program language on there own. It is important that students go though it in advance so what we can save time in the classroom.

## Link

Here are all the links for tutorials you will need during this course


### Embedded C
https://www.linkedin.com/learning/c-programming-for-embedded-applications/memory?u=57075649

### AVR 
https://www.youtube.com/playlist?list=PLA6BB228B08B03EDD

### C programming

https://www.linkedin.com/learning/learning-c-5/welcome?u=57075649
https://www.linkedin.com/learning/c-essential-training-1-the-basics/working-the-c-development-cycle?u=57075649

https://www.linkedin.com/learning/advanced-c-programming/welcome?u=57075649

### C debugging

https://www.linkedin.com/learning/ethical-hacking-exploits/welcome?u=57075649

