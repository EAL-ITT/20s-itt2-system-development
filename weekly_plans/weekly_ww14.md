---
Week: 14
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 14 ITT2 System Development

## Goals of the week(s)

PWM and ADC in ATMEGA - general presentation and interfacing

### Practical goals

1. Learn detailed implemetation of PWM.
2. Learn detailed implemetation of ADC.
3. Recap on UART.

### Learning goals

1. On cheap design.



## Deliverable

1. Blink LED on students own desgin board via uarts.
2. Generat and control PWM via UART.

## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments

