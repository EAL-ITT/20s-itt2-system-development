---
Week: 24
Content:  online plan
Material: See links in weekly plan
Initials: RUTR
---


# Google Meet info

We will be using Zoom. Details are given below.

Join ZOOM Meeting:
https://ucldk.zoom.us/j/5132567923?pwd=RXowTmtVRnNoUFhpMWV2akFiOFBlZz09
 
Join by phone:
Dial: +45 32 72 80 10    

Meeting ID: 513 256 7923
Password    :938971

Note, you will need to log in to Zoom. In order to join the meeting. Login is done with your UCL account. See https://ucl-lte.screenstepslive.com/s/22472/m/93800 for details.


## Goals of the week(s)

We will have a look again at your work, students need to present their work. Via sharing their screen. And they can receive feedback from the class. There is no - you should have worked or any other kind of critic. Students receive the help they need. 


## Practical goals

1. RUTR shows a the magic of diagrams. Using UML and other tools.
2. Students present what they have done or not done, and everyone helps. 

 
## Learning goals

1. Leaning to present our work online.
2. Structure and feedback.

## Deliverable

1. Student presentation.

## Schedule

See Time Edit

## Hands-on time

None

## Links to Material
I will be posting a link most likely in google drive, with the recording of our session. If needed.
