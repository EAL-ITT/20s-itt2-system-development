---
Week: 16
Content:  online plan
Material: See links in weekly plan
Initials: RUTR
---

# COVID-19 ITT2 Information

The actuall lecture plan has been updatedd


# Google Meet info

We will be using Google Meet for our hangout. The link is [https://meet.google.com/jom-faxi-xji](https://meet.google.com/jom-faxi-xji)

# Project outline

Due to the lock down a smaller scale project will be implemented by the students. Week 17 and Week 18 will be presentation Week with week 18 been the OLA21 second attempt. Below the goals of the project are outlined.

## Practical goals


1. Tasks for the students.
* Implement sensor / push button on explain board. Connect a button to a GPIO.
* Implement LED for pressed button. LED to GPIO, not a LED on the same GPIO as the button.
* Serial Communication from explain to RPI and back. Send button pressed command.
* Register Button presed on the RPI.
* User options for button to turn on and off other LED not - button connected LED. Rested button connected LED.
* Communication LED. Show status with blinking of communication is happening.
2. Using wifi connected the RPI to your network.
3. Create a software on your workstation to control your RPI, TCP / IP shockets are a good start.
4. Control the explain board from your work station via the RPI as a gateway.
5. Record/Keep communication data in a databases for example firebase.
6. Students can refer to the images below for a system overview.
7. Those are the minimum requirements. Students are encouraged to add more to the project. They can take in from other classes and specialization. In order to raise complexity.

 
## Learning goals

All the above points are the plan while we are under COVID - 19 lock down. Not only the goals for one day :-)

### Learning goals

1. Structre of work.
2. Work off site.
3. Project around IoT.
4. General knowledge around non TCP / IP network.
5. C programming, and Python.


## Set up

As agreed with the students on the 29th of February the project outline is been updated. The project will have steps and various levels to accommodate variate of level. The level and outline needs / goals are given below.

1. All students will build an simple sensory systems. Based on ATMEGA and using embedded C. The micro processor will be interfaced with Analog input, sensor, and controlling a motor via an H-Bridge using PWM. The ATMEGA will communicate using UART or SPI with a RPI and data will be presented using NODE RED. Allocated time is 2 weeks / two working days for students with confidence, or 5 weeks/ 5 working days for students that feel that they need to build competences.

2. All students that complete the first point. Will build and SPI network with 3 boards / ATMEGA processors. The network will be using a master to communicate with RPI and data will be shown again in NODE RED. The sensors from part one will be distributed. One will control the motor, one will have button/sensor, and one will have the analog input.

3. Students that have completed the SPI network will move to implement the same, using either WiFi or Zigbee boards. Students are expected to buy the antenna boards. This is the project that RUTR will be focusing on and will be working from day 1. Students are advised to reach this step as soon as possible.

Students as expected to work each time and possible put extra hours to reach their goals. Students will receive support in class and there will be three student helpers to ask directly:

* Alexander With
* Nicolai Skytte
* Anton Mitkov Kamenov.

Students are asked to be active and to give feedback on this process and project as it is a task in a format not tried before. This is high risk leaning project as we can easily end up without a final working project - and it is not expected from all the students. The main goals are, learning and been active! Completing task 2 will guaranty knowledge sufficient for a good result in the final exam.

# Links to google meet

## day 1

outline system overview, compoments, system [video](https://drive.google.com/open?id=16a-v7cpp6wjhWBhLkXRnLWTwRX4BI5c_)

uart/servial [video](https://drive.google.com/open?id=1tjE3t0dV6fG2qGKKdsWtUlVs1SWCjVaZ)

## day 2

some extra from day 1 that we talked about the second day [video](https://drive.google.com/open?id=1sdIyjYCzK0DcfB0x3-Z9P7_Yhep7JerI)

system over view, serial, embedded design [video](https://drive.google.com/open?id=1ICLdhkXQicaq7OJvppBsvdRNtLDALNJ8)

## day 3

PWM and Theory [video](https://drive.google.com/open?id=1fA27LLYkA__gsPItvj0_CtnI8dmXGmBd)



### PWM video

I will be uploading a video tutorial on PWM with AVR.


### Serial controlled PWM

I will be making a video tutorial on serially controlling a pwm


## Video examples

[Here is a video example from Anton](https://www.youtube.com/watch?time_continue=209&v=EBPJogWP1_E&feature=emb_logo). The video shows a system control via
and interface, user based, application. You can ask him directly of all the issues.


## Video tutorial

Here are two video tutorials [one for pwm](https://drive.google.com/open?id=1QrOJHDMqtugbkYKOSZD_Uw6DAQtJei4P) and [one for uart](https://drive.google.com/open?id=14v9-sO0mXMs4kQvsOqUNcBWUn8EvwrNZ). Please keep in mind two things the sound is horrible. I tested two microphones in both video, but there is too much echo and bad sounds due to where I sit. Second there are other noises in the background, kids banging on the doors, ect. I found out that lowering the volume and play the video on a bit higher speed solves some of the worst issues.


# Pictues Diagrams and drawings


![AMA vs ACM](1.png)

![System Overview](2.png)

![System simple](3.png)

![Serial with and without level shifter](4.png)

![Serial Simple USB plug and play](5.png)

![Overview transistor as switch](6.png)

![Transistor as switch comlpex](10.png)

![Overview ADC](7.png)

![PWM theory 1](8.png)

![PWM theory 2](9.png)
