---
Week: 12
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 12 ITT2 System Development

## Goals of the week(s)

UART communication, this is the most important communication mode that we will use. At it is the key for communicating with other devices, for example the RPI and other atmega controllers.

### Practical goals

Inventory of compoments - students are asked to know what they have in hand.
2. Setting up the way we will do the class - this will be a class discussion.
3. Presentation of UART - Communication between RPI and ATMEGA explain board.

### Learning goals

1. Structre of work.
2. Work off site.
3. Project around IoT.
4. General knowledge around non TCP / IP network.
5. C programming, and Python.
5. UART Communication


## Deliverable

1. Create a link between RPI and ATMEGA using UART.
2. Create a link between ATMEGA and ATMEGA.


## Schedule

See Time Edit

## Hands-on time

All hand-ins are in Gitlab. Students are expected to hand in their Deliverable.

## Comments

